import os
import constants
import config
import load_data
import datasets
import models
import trainers


def main():
    # Load img files
    train_img_files, val_img_files = load_data.load_img_files(
        data_dir=constants.DATA_DIR,
        train_size=config.TRAIN_SIZE,
        val_size=config.VAL_SIZE)

    # Create train and val datasets
    train_dataset = datasets.SimCLRDataset(
        img_files=train_img_files,
        batch_size=config.BATCH_SIZE,
        delta_hue=config.DELTA_HUE,
        resize_shape=config.RESIZE_SHAPE,
        scaling=config.SCALING,
        num_steps_per_epoch=config.NUM_STEPS_PER_EPOCH,
        shuffle=config.SHUFFLE,
        drop_remainder=config.DROP_LAST_BATCH)

    val_dataset = datasets.SimCLRDataset(
        img_files=val_img_files,
        batch_size=config.BATCH_SIZE,
        delta_hue=config.DELTA_HUE,
        resize_shape=config.RESIZE_SHAPE,
        scaling=config.SCALING,
        num_steps_per_epoch=config.NUM_BATCHES_FOR_EVALUATION,
        shuffle=False,
        drop_remainder=False)

    # Create model
    base_model_class = models.get_model_class(config.MODEL_NAME)
    resnet_simclr = models.ResnetSimCLR(
        base_model_class=base_model_class,
        projected_features_dim=config.PROJECTED_FEATURES_DIM)

    # Train the model
    trainer = trainers.SimCLRTrainer()
    trainer.train(
        model=resnet_simclr,
        train_dataset=train_dataset,
        val_dataset=val_dataset,
        training_params={
            "TEMPERATURE": config.TEMPERATURE,
            "EPOCHS": config.EPOCHS,
            "LEARNING_RATE": config.LEARNING_RATE,
            "MIN_LEARNING_RATE": config.MIN_LEARNING_RATE,
            "COSINE_DECAY_LR_STEPS": config.COSINE_DECAY_LR_STEPS,
            "PATIENCE_EARLY_STOPPING": config.PATIENCE_EARLY_STOPPING
        },
        validation_params={
            "TEMPERATURE": config.TEMPERATURE
        })

    # Save base_model without the head
    print("Dumping trained weights")
    os.makedirs("./checkpoints", exist_ok=True)
    resnet_simclr.base_model.save_weights("./checkpoints/{}.h5".format(
        config.MODEL_CHECKPOINT_NAME))

    # Sanity check by creating a base_model load weights obtained from SimCLR
    # training.
    print("Creating and building other model")
    base_model = base_model_class(
        classes=5,
        include_top=False)
    base_model.build((None, 128, 128, 3))
    print("Loading dumped weights")
    base_model.load_weights("./checkpoints/{}.h5".format(
        config.MODEL_CHECKPOINT_NAME))
    print("Loading weights successed")


if __name__ == '__main__':
    main()
