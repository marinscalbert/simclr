import numpy as np
import tensorflow as tf
import transforms


class SimCLRDataset():
    """Two stream dataset used for training a model with SimCLR.

    Args:
        img_files (list): List of file paths.
        batch_size (int): Batch size used during training
        delta_hue (float): Specify the range of hue [-delta_hue], +delta_hue]
            for random hue transformation
        resize_shape (tuple): Reshape size after random cropping. Should be
            specified as (H, W).
        scaling (float): Preprocessing scaling parameter
        num_steps_per_epoch (int): Number of steps per epoch
        shuffle (bool): Boolean indicating whether or not shuffling the data
            between each epoch.
        drop_remainder (bool): Boolean indicating whether or not droping the
            last batch if the batch does not contain batch_size elements.

    Attributes:
        img_files (list): List of file paths.
        batch_size (int): Batch size used during training
        delta_hue (float): Specify the range of hue [-delta_hue], +delta_hue]
            for random hue transformation
        resize_shape (tuple): Reshape size after random cropping. Should be
            specified as (H, W).
        scaling (float): Preprocessing scaling parameter
        num_steps_per_epoch (int): Number of steps per epoch
        shuffle (bool): Boolean indicating whether or not shuffling the data
            between each epoch.
        drop_remainder (bool): Boolean indicating whether or not droping the
            last batch if the batch does not contain batch_size elements.
        transforms (transforms.TransformationsCLR):
            object whose __call__ function takes an img tensor and apply
            several transformations (random_hue, random_cropping, resize,
            random_gaussian_blur).
        dataset (tf.data.Dataset): Tensorflow Dataset object.
        num_batches (int): The number of batches per epoch.

    """

    def __init__(
            self,
            img_files,
            batch_size,
            delta_hue=0.2,
            resize_shape=None,
            scaling=None,
            num_steps_per_epoch=None,
            shuffle=False,
            drop_remainder=False):
        super(SimCLRDataset, self).__init__()
        self.img_files = img_files
        self.batch_size = batch_size
        self.delta_hue = delta_hue
        self.resize_shape = resize_shape
        self.scaling = scaling
        self.num_steps_per_epoch = num_steps_per_epoch
        self.shuffle = shuffle
        self.drop_remainder = drop_remainder
        self.transforms = transforms.TransformationsCLR(
            delta_hue=self.delta_hue,
            resize_shape=self.resize_shape)

        self.dataset = self.build_dataset()
        if self.num_steps_per_epoch is not None:
            self.num_batches = num_steps_per_epoch
        else:
            self.num_batches = np.floor(len(img_files)/batch_size).astype(int)

    def _generator(self):
        for i in range(len(self.img_files)):
            yield self.img_files[i]

    def read_and_preprocess(self, filename):
        img = tf.io.read_file(filename)
        img = tf.image.decode_jpeg(img)
        img = tf.cast(img, tf.float32)
        if self.scaling is not None:
            img = img*self.scaling
        return img, img

    def build_dataset(self):

        dataset = tf.data.Dataset.from_generator(
            generator=self._generator,
            output_types=(tf.dtypes.string))

        # Read data and preprocess raw images
        dataset = dataset.map(
            lambda x: (self.read_and_preprocess(x)),
            num_parallel_calls=tf.data.experimental.AUTOTUNE)

        # Get a jigsaw puzzle from raw images and the permutation associated
        # to the puzzle.
        dataset = dataset.map(
            lambda x1, x2: (self.transforms(x1), self.transforms(x2)),
            num_parallel_calls=tf.data.experimental.AUTOTUNE)

        if self.shuffle:
            dataset = dataset.shuffle(
                buffer_size=10000,
                reshuffle_each_iteration=True)

        dataset = dataset.batch(
            self.batch_size,
            drop_remainder=self.drop_remainder)

        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
        if self.num_steps_per_epoch is not None:
            dataset = dataset.take(self.num_steps_per_epoch)

        return dataset
