import tensorflow as tf
import numpy as np


def gaussian_blur(image, kernel_size, sigma, padding='SAME'):
    """Blurs the given image with separable convolution.


    Args:
        image: Tensor of shape [height, width, channels] and dtype float to
            blur.
        kernel_size: Integer Tensor for the size of the blur kernel. This is
            should be an odd number. If it is an even number, the actual kernel
            size will be size + 1.
      sigma: Sigma value for gaussian operator.
      padding: Padding to use for the convolution. Typically 'SAME' or 'VALID'.

    Returns:
      A Tensor representing the blurred image.
    """
    radius = tf.cast(kernel_size / 2, tf.int32)
    kernel_size = radius * 2 + 1
    x = tf.cast(tf.range(-radius, radius + 1), tf.float32)
    blur_filter = tf.exp(
        -tf.pow(x, 2.0) / (2.0 * tf.pow(tf.cast(sigma, tf.float32), 2.0)))
    blur_filter /= tf.reduce_sum(blur_filter)
    # One vertical and one horizontal filter.
    blur_v = tf.reshape(blur_filter, [kernel_size, 1, 1, 1])
    blur_h = tf.reshape(blur_filter, [1, kernel_size, 1, 1])
    num_channels = tf.shape(image)[-1]
    blur_h = tf.tile(blur_h, [1, 1, num_channels, 1])
    blur_v = tf.tile(blur_v, [1, 1, num_channels, 1])
    expand_batch_dim = image.shape.ndims == 3
    if expand_batch_dim:
        # Tensorflow requires batched input to convolutions, which we can fake
        # with an extra dimension.
        image = tf.expand_dims(image, axis=0)
    blurred = tf.nn.depthwise_conv2d(
        image, blur_h, strides=[1, 1, 1, 1], padding=padding)
    blurred = tf.nn.depthwise_conv2d(
        blurred, blur_v, strides=[1, 1, 1, 1], padding=padding)
    if expand_batch_dim:
        blurred = tf.squeeze(blurred, axis=0)
    return blurred


class TransformationsCLR():
    """Class implementing the set of transformations to be applied during SimCLR
    training.

    Args:
        delta_hue (float): Specify the range of hue [-delta_hue], +delta_hue]
            for random hue transformation
        resize_shape (tuple): Reshape size after random cropping. Should be
            specified as (H, W).

    Attributes:
        delta_hue (float): Specify the range of hue [-delta_hue], +delta_hue]
            for random hue transformation
        resize_shape (tuple): Reshape size after random cropping. Should be
            specified as (H, W).

    """

    def __init__(
            self,
            delta_hue,
            resize_shape):
        self.delta_hue = delta_hue
        self.resize_shape = resize_shape

    def __call__(self, x):
        # Color jittering
        x = tf.image.random_hue(
            x,
            max_delta=self.delta_hue)

        # Random crop and resize
        H, W = tf.shape(x)[0], tf.shape(x)[1]
        x = tf.image.random_crop(
            x,
            [H-np.random.randint(15), W-np.random.randint(15), 3])
        x = tf.image.resize(
            x,
            self.resize_shape,
            method=tf.image.ResizeMethod.BILINEAR,
            antialias=True)

        # Random flips and rotation by multiple of 90°
        x = tf.image.random_flip_up_down(x)
        x = tf.image.random_flip_left_right(x)
        x = tf.image.rot90(x, k=tf.random.uniform(
            shape=[1, ], minval=0, maxval=4, dtype=tf.int32)[0])

        # Random gaussian blur
        choice = tf.random.uniform(
            shape=[1, ], minval=0., maxval=1., dtype=tf.float32)[0]
        x = tf.cond(choice < 0.5, lambda: x, lambda: gaussian_blur(
            image=x,
            kernel_size=3,
            sigma=tf.cast(
                tf.random.uniform(shape=[1, ], minval=2, maxval=6.)[0],
                tf.int32)))
        return x
