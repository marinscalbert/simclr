# Configuration for split
TRAIN_SIZE = 0.99
VAL_SIZE = 0.01

# Configuration for model
MODEL_NAME = "ResNet18"
PROJECTED_FEATURES_DIM = 512

# Configuration for preprocessing and data augmentation
DELTA_HUE = 0.20
RESIZE_SHAPE = (128, 128)
SCALING = 1/255.

# Configuration for training
EPOCHS = 2000
BATCH_SIZE = 600
LEARNING_RATE = 1e-4
NUM_STEPS_PER_EPOCH = 200
SHUFFLE = True
DROP_LAST_BATCH = True
MIN_LEARNING_RATE = 1e-6
COSINE_DECAY_LR_STEPS = 1000
PATIENCE_EARLY_STOPPING = 20
TEMPERATURE = 0.5

# Configuration for evaluation
# None if all batches should be used
NUM_BATCHES_FOR_EVALUATION = 100

# Configuration for model checkpoint
MODEL_CHECKPOINT_NAME = "resnet18_simclr_512"
