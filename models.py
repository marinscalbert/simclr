import tensorflow as tf
from blocks import make_basic_block_layer, make_bottleneck_layer


def get_model_class(model_name):
    if model_name == "ResNet18":
        return ResNet18
    elif model_name == "ResNet34":
        return ResNet34
    elif model_name == "ResNet50":
        return ResNet50
    elif model_name == "ResNet101":
        return ResNet101
    elif model_name == "ResNet152":
        return ResNet152
    else:
        raise ValueError(
                "Cannot get {} model because it is not implemented".format(
                    model_name))


class ResNetTypeI(tf.keras.models.Model):

    def __init__(
            self,
            classes,
            layer_params,
            include_top=True):
        super(ResNetTypeI, self).__init__()
        self.include_top = include_top
        self.classes = classes

        self.conv1 = tf.keras.layers.Conv2D(filters=64,
                                            kernel_size=(7, 7),
                                            strides=2,
                                            padding="same")
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.pool1 = tf.keras.layers.MaxPool2D(pool_size=(3, 3),
                                               strides=2,
                                               padding="same")

        self.layer1 = make_basic_block_layer(filter_num=64,
                                             blocks=layer_params[0])
        self.layer2 = make_basic_block_layer(filter_num=128,
                                             blocks=layer_params[1],
                                             stride=2)
        self.layer3 = make_basic_block_layer(filter_num=256,
                                             blocks=layer_params[2],
                                             stride=2)
        self.layer4 = make_basic_block_layer(filter_num=512,
                                             blocks=layer_params[3],
                                             stride=2)

        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
        if include_top:
            self.fc = tf.keras.layers.Dense(
                units=classes, activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pool1(x)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        if self.include_top:
            x = self.fc(x)
        return x

    def compute_output_shape(self, input_shape):
        if self.include_top:
            return (input_shape[0], self.classes)
        else:
            return (input_shape[0], 512)


class ResNetTypeII(tf.keras.models.Model):

    def __init__(
            self,
            classes,
            layer_params,
            include_top=True):
        super(ResNetTypeII, self).__init__()
        self.include_top = include_top

        self.conv1 = tf.keras.layers.Conv2D(filters=64,
                                            kernel_size=(7, 7),
                                            strides=2,
                                            padding="same")
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.pool1 = tf.keras.layers.MaxPool2D(pool_size=(3, 3),
                                               strides=2,
                                               padding="same")

        self.layer1 = make_bottleneck_layer(filter_num=64,
                                            blocks=layer_params[0])
        self.layer2 = make_bottleneck_layer(filter_num=128,
                                            blocks=layer_params[1],
                                            stride=2)
        self.layer3 = make_bottleneck_layer(filter_num=256,
                                            blocks=layer_params[2],
                                            stride=2)
        self.layer4 = make_bottleneck_layer(filter_num=512,
                                            blocks=layer_params[3],
                                            stride=2)

        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
        if self.include_top:
            self.fc = tf.keras.layers.Dense(
                units=classes, activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pool1(x)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        if self.include_top:
            x = self.fc(x)

        return x

    def compute_output_shape(self, input_shape):
        if self.include_top:
            return (input_shape[0], self.classes)
        else:
            return (input_shape[0], 2048)


class ResNet18(ResNetTypeI):

    def __init__(
            self,
            classes,
            include_top=True):
        super(ResNet18, self).__init__(
            classes=classes,
            layer_params=[2, 2, 2, 2],
            include_top=include_top)


class ResNet34(ResNetTypeI):

    def __init__(
            self,
            classes,
            include_top=True):
        super(ResNet34, self).__init__(
            classes=classes,
            layer_params=[3, 4, 6, 3],
            include_top=include_top)


class ResNet50(ResNetTypeII):

    def __init__(
            self,
            classes,
            include_top=True):
        super(ResNet50, self).__init__(
            classes=classes,
            layer_params=[3, 4, 6, 3],
            include_top=include_top)


class ResNet101(ResNetTypeII):

    def __init__(
            self,
            classes,
            include_top=True):
        super(ResNet101, self).__init__(
            classes=classes,
            layer_params=[3, 4, 23, 3],
            include_top=include_top)


class ResNet152(ResNetTypeII):

    def __init__(
            self,
            classes,
            include_top=True):
        super(ResNet152, self).__init__(
            classes=classes,
            layer_params=[3, 8, 36, 3],
            include_top=include_top)


class ResnetSimCLR(tf.keras.models.Model):

    def __init__(
            self,
            base_model_class,
            projected_features_dim=1024):
        super(ResnetSimCLR, self).__init__()
        self.base_model = base_model_class(
            classes=None,
            include_top=False
        )
        self.fc1 = tf.keras.layers.Dense(
            units=projected_features_dim, use_bias=False, activation="relu")
        self.fc2 = tf.keras.layers.Dense(
            units=projected_features_dim, activation="linear", use_bias=False)

    def call(self, x, training=None):
        x = self.base_model(x, training=training)
        x = self.fc1(x)
        x = self.fc2(x)
        return x
