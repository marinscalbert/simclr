# SimCLR - A Simple Framework for Contrastive Learning of Visual Representations

This project implements the self-supervised framework SimCLR detailed in this
<a href="https://arxiv.org/abs/2002.05709">paper</a>.

<img src="https://miro.medium.com/max/2000/1*1uaA1tE5PDnVpSljxSTEoQ.png" alt="SimCLR framework diagram" scale="0.5">

## Get started

### Set up environment

#### Via conda

1. Create the conda environment

```bash
conda create -n simclr python=3.7 \
		tqdm \
		tensorflow \
		scikit-learn
```

2. Activate the conda environment

```bash
conda activate simclr
```

3. For GPUs users only

```bash
conda install -c anaconda tensorflow-gpu
```

### Set configurations

The data dir should be specified in `constants.py`.
<br>
The configuration can be modified through the file `config.py`.

### Train a model with SimCLR

After activating the environment and setting all the configurations in `constants.py` and `config.py`. Run :
```bash
python main.py
```

### Transfer learning from a model trained with SimCLR

At the end of the training with SimCLR framework, the encoder weights are saved and the head weights discarded. Now, if we want to use the encoder part for transfer learning, we just need to recreate a base_model and load the weights like the following :

```python
import models

# Creating the base model and remove the top fully connected
pretrained_model = models.ResNet18(
    classes=5,
    include_top=False)
# Build the model and load pretrained weights
pretrained_model.build((None, None, None, 3))
pretrained_model.load_weights("./checkpoints/my_model_name.h5")
```
