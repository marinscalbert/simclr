import tqdm
import tensorflow as tf
import losses


class Evaluator():
    """Class for evaluating a model trained with simCLR framework."""

    def __init__(
            self):
        super(Evaluator, self).__init__()

    @tf.function
    def eval_step(
            self,
            model,
            X1,
            X2,
            ntxent_loss):

        z1 = model(X1, training=False)
        z2 = model(X2, training=False)
        loss = ntxent_loss(z1, z2)
        return loss

    def evaluate(
            self,
            model,
            val_dataset,
            validation_params):

        # Initialize losses
        val_loss_mean = tf.keras.metrics.Mean()
        ntxent_loss = losses.NTXent(
            temperature=validation_params["TEMPERATURE"])

        pbar = tqdm.tqdm(
            val_dataset.dataset,
            unit="batch",
            total=val_dataset.num_batches)

        for X1, X2 in pbar:

            loss = self.eval_step(
                model=model,
                X1=X1,
                X2=X2,
                ntxent_loss=ntxent_loss)
            val_loss_mean.update_state(loss)
        return val_loss_mean.result()
