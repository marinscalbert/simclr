import tqdm
import tensorflow as tf

import losses
from callbacks import (
    CosineDecayLearningRateScehuler,
    EarlyStopping)
from evaluators import Evaluator


class SimCLRTrainer():

    def __init__(self):
        pass

    @tf.function
    def train_step(
            self,
            model,
            optimizer,
            X1,
            X2,
            ntxent_loss,
            training_params):
        # Compute loss and gradients
        with tf.GradientTape() as tape:
            # Compute loss
            z1 = model(X1, training=True)
            z2 = model(X2, training=True)

            loss = ntxent_loss(
                z1, z2)

            grads = tape.gradient(
                loss, model.trainable_variables)

            # Update weights
            optimizer.apply_gradients(
                zip(grads, model.trainable_variables))

        return loss, grads

    def train(
            self,
            model,
            train_dataset,
            val_dataset,
            training_params,
            validation_params):

        # Initialize metrics
        train_total_loss = tf.keras.metrics.Mean(name="train_total_loss")

        # Initialize losses
        ntxent_loss = losses.NTXent(
            temperature=training_params["TEMPERATURE"])

        # Define optimizer
        optimizer = tf.keras.optimizers.Adam(
            lr=training_params["LEARNING_RATE"])

        # Create callbacks
        cosine_decay_lr_scheduler = CosineDecayLearningRateScehuler(
            optimizer=optimizer,
            lr_init=training_params["LEARNING_RATE"],
            lr_final=training_params["MIN_LEARNING_RATE"],
            decay_steps=training_params["COSINE_DECAY_LR_STEPS"])
        early_stopping = EarlyStopping(
            model=model,
            patience=training_params["PATIENCE_EARLY_STOPPING"])

        for epoch in range(0, training_params["EPOCHS"]):

            pbar = tqdm.tqdm(
                train_dataset.dataset,
                unit="batch",
                desc="Epoch {}/{}, tot_loss: {}".format(
                    epoch+1,
                    training_params["EPOCHS"],
                    None),
                total=train_dataset.num_batches)

            # Training for one epoch
            for X1, X2 in pbar:

                loss, grads = self.train_step(
                    model=model,
                    optimizer=optimizer,
                    X1=X1,
                    X2=X2,
                    ntxent_loss=ntxent_loss,
                    training_params=training_params)

                train_total_loss.update_state(loss)
                pbar.set_description(
                    "Epoch {}/{}, loss: {:.4f}".format(
                        epoch+1,
                        training_params["EPOCHS"],
                        train_total_loss.result()))

            # Validation step
            evaluator = Evaluator()
            val_loss = evaluator.evaluate(
                model,
                val_dataset,
                validation_params)
            print("Val loss: {:.4f}".format(val_loss))

            cosine_decay_lr_scheduler.on_epoch_end(epoch+1)
            early_stopping.on_epoch_end(val_loss)
            if early_stopping.stop_training:
                break
