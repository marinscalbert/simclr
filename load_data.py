# coding: utf-8

import os
import glob
from sklearn.model_selection import train_test_split


def load_img_files(data_dir, train_size, val_size):
    """Retrieve img paths and split them into train and val dataset.

    Args:
        data_dir (str): Directory contains the unlabeled images
            data_dir is assumed to be organized as follow :
            data_dir
                ├── img1.jpeg
                ├── img2.jpeg
                └── img3.jpeg
        train_size (float): Proportion of the images to used for the train
            dataset.
        val_size (float): Proportion of the images to used for the val dataset.

    Returns:
        tuple: A tuple containing the list of images path for training and the
            list of images path for validation.
    """

    img_files = glob.glob(
        os.path.join(data_dir, "*.jpeg"))
    train_img_files, val_img_files = train_test_split(
        img_files, train_size=train_size, test_size=val_size)
    return train_img_files, val_img_files
