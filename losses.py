import tensorflow as tf


def pairwise_cosine_similarity(y1, y2, normalize=False):
    """Compute the pairwise cosine similarity between tensors y1 and y2.

    Args:
        y1 (tf.Tensor):
            Tensor y1 of shape (M, D).
        y2 (tf.Tensor):
            Tensor y2 of shape (N, D).
        normalize (bool): Boolean indicating wether or not to normalize the
            tensors y1 and y2. If true the euclidean norm is used.

    Returns:
        tf.Tensor:
            Pairwise cosine similarity matrix

    Examples:
        Examples should be written in doctest format, and
        should illustrate how to use the function/class.
        >>> y1 = [[0,1], [1,0]]
        >>> y2 = [[-1,0], [0,-1]]
        >>> d = pairwise_cosine_similarity(y1, y2)
        >>> print(d)
        [[ 0., -1.],
        [-1.,  0.]]
    """
    if normalize:
        y1_norm = tf.maximum(1e-12, tf.norm(y1, axis=-1, keepdims=True))
        y2_norm = tf.maximum(1e-12, tf.norm(y2, axis=-1, keepdims=True))
        y1 = y1/y1_norm
        y2 = y2/y2_norm
    d = tf.matmul(y1, y2, transpose_b=True)
    return d


class NTXent(tf.keras.losses.Loss):
    """Class for custom Tensorflow loss for computing the normalized
        temperature-scaled cross entropy loss specified in
        "https://arxiv.org/pdf/2002.05709.pdf".

    Args:
        temperature (float): Description of parameter `temperature`.

    Attributes:
        temperature (float): Description of parameter `temperature`.

    """

    def __init__(
            self,
            temperature):
        super(NTXent, self).__init__()
        self.temperature = temperature
#

    def call(self, y1, y2):
        batch_size = tf.shape(y1)[0]

        # Normalize y1 and y2 : y1/||y1|| y2/||y2||
        y1_norm = tf.maximum(1e-12, tf.norm(y1, axis=-1, keepdims=True))
        y2_norm = tf.maximum(1e-12, tf.norm(y2, axis=-1, keepdims=True))
        y1 = y1/y1_norm
        y2 = y2/y2_norm


        # Compute similarity onfor all examples y1*y2/(||y1||*||y2||)
        Y = tf.concat([y1, y2], axis=0)
        sim_matrix = pairwise_cosine_similarity(Y, Y, normalize=False)

        # get positive pairs mask and negative pairs mask
        identity = tf.eye(batch_size)
        zeros = tf.zeros((batch_size, batch_size))
        pos_pairs_mask = tf.concat([
            tf.concat([zeros, identity], axis=-1),
            tf.concat([identity, zeros], axis=-1),
        ], axis=0)
        neg_pairs_mask = 1-pos_pairs_mask-tf.eye(tf.shape(pos_pairs_mask)[0])

        pos_pairs = tf.reshape(
            tf.boolean_mask(sim_matrix, pos_pairs_mask),
            [tf.shape(sim_matrix)[0], -1])

        neg_pairs = tf.reshape(
            tf.boolean_mask(sim_matrix, neg_pairs_mask),
            [tf.shape(sim_matrix)[0], -1])


        pairs = tf.concat([pos_pairs, neg_pairs], axis=-1)/self.temperature
        pairs = tf.nn.softmax(pairs, axis=-1)
        losses = -tf.math.log(pairs[:,0])
        loss = tf.reduce_mean(losses)
        return loss


class NTXentLoss(tf.keras.losses.Loss):
    """Class for custom Tensorflow loss for computing the normalized
        temperature-scaled cross entropy loss specified in
        "https://arxiv.org/pdf/2002.05709.pdf".
        The loss is computer from the sparse_categorical_crossentropy tf
        function.

    Args:
        temperature (float): Description of parameter `temperature`.

    Attributes:
        temperature (float): Description of parameter `temperature`.

    """

    def __init__(
            self,
            temperature):
        super(NTXentLoss, self).__init__()
        self.temperature = temperature
#

    def call(self, y1, y2):
        # Normalize y1 and y2 : y1/||y1|| y2/||y2||
        y1_norm = tf.maximum(1e-12, tf.norm(y1, axis=-1, keepdims=True))
        y2_norm = tf.maximum(1e-12, tf.norm(y2, axis=-1, keepdims=True))
        y1 = y1/y1_norm
        y2 = y2/y2_norm

        # Compute similarity on positive pairs y1*y2/(||y1||*||y2||)
        pos_sim = tf.reduce_sum(y1*y2, axis=-1)
        # Replicate pos sims as similarity between y2 and y1 is the same as
        # y1 and y2.
        pos_sim = tf.tile(pos_sim, [2])

        Y = tf.concat([y1, y2], axis=0)
        sim_matrix = pairwise_cosine_similarity(Y, Y)
        mask = tf.cast(1-tf.eye(sim_matrix.shape[0]), tf.bool)
        neg_sim_matrix = tf.reshape(
            tf.boolean_mask(sim_matrix, mask),
            [sim_matrix.shape[0], -1])
        pos_neg_sim = tf.concat(
            [tf.expand_dims(pos_sim, axis=-1), neg_sim_matrix], axis=-1)/self.temperature
        targets = tf.zeros(pos_neg_sim.shape[0], dtype=tf.int32)
        losses = tf.keras.losses.sparse_categorical_crossentropy(
            y_true=targets,
            y_pred=pos_neg_sim,
            from_logits=True,
            axis=-1)
        loss = tf.reduce_mean(losses)
        return loss
